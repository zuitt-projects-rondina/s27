// Node Js routing with HTTP Methods

// CRUD OPERATIONS      HTTP Methods
	// C -create        POST
	// R -read          GET 
	// U -update        PUT / PATCH
	// D -delete		DELETE


const http = require("http");

// Mock data for users and coures

let users = [
		{
			username : "peterIsHomeless",
			email : "peterParker@mail.com",
			password : "peterNoWayHome"
		},
		{
			username : "Tony3000",
			email : "starksIndustries@mail.com",
			password : "ironManWillBeBack"
		}

];

let courses = [
	{
			name : "Math 103",
			price : 2500,
			isActive : true
	},
	{
			name : "Biology 201",
			price : 2500,
			isActive : true
	},
];

http.createServer((req, res) =>{

	if(req.url === "/" && req.method === "GET"){
		res.writeHead(200, {'Content-Type' : 'text/plain'});
		res.end('This route is for checking GET method')

	} else if(req.url === "/" && req.method === "POST"){
		res.writeHead(200, {'Content-Type' : 'text/plain'});
		res.end('This route is checking a POST method')

	} else if(req.url === "/" && req.method === "PUT"){
		res.writeHead(200, {'Content-Type' : 'text/plain'});
		res.end('This route is checking a PUT method')

	} else if(req.url === "/" && req.method === "DELETE"){
		res.writeHead(200, {'Content-Type' : 'text/plain'});
		res.end('This route is checking a DELETE method')

	} else if(req.url === "/users" && req.method === "GET"){

		//Change the value of Content-Type header if we are passing json as our server response : 'application/json'
		res.writeHead(200, {'Content-Type' : 'application/json'});

		// We cannot pass other data type a a response except for a string
		// To be able to pass the array of users, first we stringify the array as JSON
		res.end(JSON.stringify(users));

	} else if(req.url === "/courses" && req.method === "GET"){
		res.writeHead(200, {'Content-Type' : 'application/json'});
		res.end(JSON.stringify(courses));


	} else if(req.url === "/users" && req.method === "POST"){

		let requestBody = "";

		// Receiving data from client to nodejs server requires 2 steps:

		// data step- this part will read the stream of data from our client and process the incoming data into the requestBody variable

		req.on('data', (data) => {

			console.log(data.toString())

			requestBody += data
		})

		// end step- will run once or after the request has been completely sent from our client

		req.on('end', () => {

			console.log(requestBody)

			requestBody = JSON.parse(requestBody)

			let newUser = {

				username: requestBody.username,
				email: requestBody.email,
				password: requestBody.password

			}

			users.push(newUser);
			console.log(users);

			res.writeHead(200, {'Content-Type' : 'application/json'})
			res.end(JSON.stringify(users))
		})


 } else if(req.url === "/courses" && req.method === "POST"){

 			let newRequestBody = "";

 			req.on('data',(data) =>{

 				console.log(data.toString());
 				newRequestBody += data	

 			})

 			req.on('end', () =>{
 				console.log(newRequestBody);

 				newRequestBody = JSON.parse(newRequestBody)

 				let newCourse = {

 					name : newRequestBody.name,
 					price : newRequestBody.price,
 					isActive : newRequestBody.isActive
 				}

 				courses.push(newCourse);
 				console.log(courses)

 				res.writeHead(200, {'Content-Type' : 'application/json'})
				res.end(JSON.stringify(courses))



 			})


 }

}).listen(4000);
console.log('Server is running on localhost:4000')


/*
	MINI ACTIVITY 
*/